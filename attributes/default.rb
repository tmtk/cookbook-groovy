home = "#{ENV['HOME']}/.groovy"
default[:groovy]["2.1.0"][:release] = "http://dist.groovy.codehaus.org/distributions/groovy210/groovy-binary-2.1.0.zip"
default[:groovy][:release]          = default.groovy["2.1.0"].release
default[:groovy][:home]             = home + "/" + (::File.basename default.groovy.release, ".zip")
default[:groovy][:archive_path]     = home + "/" + (::File.basename default.groovy.release)
