
remote_file node.groovy.archive_path do
  source node.groovy.release
  not_if {::File.exists? node.groovy.archive_path}
end

execute "unzip -u #{node.groovy.archive_path}" do
  cwd "#{File.expand_path "..", node.groovy.home}"
end

